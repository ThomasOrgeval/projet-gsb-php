<?php

if (isset($_POST['connexion'])) {
    $name = htmlspecialchars($_POST['pseudo']);
    $password = htmlspecialchars($_POST['pass']);

}

// Sélection de la base de donnée

function db()
{
    $bdd = new PDO("mysql:host=127.0.0.1;dbname=gsbv2", "root", "");
    return $bdd;
}

// Fonction pour la connexion 

function login()
{
    $bdd = db();
    $userexist = false;

    if ($bdd) {
        $login = htmlspecialchars($_POST['pseudo']);
        $mdp = htmlspecialchars($_POST['pass']);

        if (!empty($login) and !empty($mdp)) {
            $requser = $bdd->prepare("SELECT * FROM Visiteur WHERE login = ? AND mdp = ?");
            $requser->execute(array($login, $mdp));
            $userexist = $requser->fetch();
        }
    }

    return $userexist;
}


// Fonctions dans Frais Forfait pour la création d'une fiche


function donneesFF()
{
    $bdd = db();
    $FicheFrais = $bdd->query('SELECT * FROM LigneFraisForfait');
    $FicheFraisDonnees = $FicheFrais->fetch();

    return $FicheFraisDonnees;
}

function ligneFF()
{
    $db = db();
    $SessionID = $_SESSION['id'];
    $sql_select = "SELECT max(ID_FraisForfait) FROM `lignefraisforfait` WHERE ID_Visiteur = ('$SessionID') ";
    $st = $db->prepare($sql_select);
    $st->execute();
    $ligne = $st->fetch();

    return $ligne;
}


// Fonction dans Frais Hors-Forfait pour la création d'une fiche


function donneesFHF()
{
    $bdd = db();
    $FicheFraisHF = $bdd->query('SELECT * FROM LigneFraisHorsForfait');
    $FicheFraisHFDonnees = $FicheFraisHF->fetch();

    return $FicheFraisHFDonnees;
}

// Consultation des données pour les rentrer dans un tableau

function donneesConsulterNbrFF()
{
    $dbb = db();
    $mois = $_POST['mois'];
    $id = $_SESSION['id'];

    $query = "SELECT * FROM lignefraisforfait WHERE ID_Visiteur='$id' and Mois = '$mois'";
    try {
        $pdo_select = $dbb->prepare($query);
        $pdo_select->execute();
        $NbreDataFF = $pdo_select->rowCount();
    } catch (PDOException $e) {
        echo 'Erreur SQL : ' . $e->getMessage() . '<br/>';
        die();
    }
    return $NbreDataFF;
}

function donneesConsulterRowFF()
{
    $dbb = db();
    $mois = $_POST['mois'];
    $id = $_SESSION['id'];

    $query = "SELECT * FROM lignefraisforfait WHERE ID_Visiteur='$id' and Mois = '$mois'";
    try {
        $pdo_select = $dbb->prepare($query);
        $pdo_select->execute();
        $rowAllFF = $pdo_select->fetchAll();
    } catch (PDOException $e) {
        echo 'Erreur SQL : ' . $e->getMessage() . '<br/>';
        die();
    }
    return $rowAllFF;
}

function donnneesConsulterNbrFHF()
{
    $dbb = db();
    $mois = $_POST['mois'];
    $id = $_SESSION['id'];

    $queryFHF = "SELECT * FROM lignefraishorsforfait WHERE ID_Visiteur='$id' and Mois = '$mois'";
    try {
        $pdo_selectFHF = $dbb->prepare($queryFHF);
        $pdo_selectFHF->execute();
        $NbreDataFHF = $pdo_selectFHF->rowCount();
    } catch (PDOException $e) {
        echo 'Erreur SQL : ' . $e->getMessage() . '<br/>';
        die();
    }
    return $NbreDataFHF;
}

function donnneesConsulterRowFHF()
{
    $dbb = db();
    $mois = $_POST['mois'];
    $id = $_SESSION['id'];

    $queryFHF = "SELECT * FROM lignefraishorsforfait WHERE ID_Visiteur='$id' and Mois = '$mois'";
    try {
        $pdo_selectFHF = $dbb->prepare($queryFHF);
        $pdo_selectFHF->execute();
        $rowAllFHF = $pdo_selectFHF->fetchAll();
    } catch (PDOException $e) {
        echo 'Erreur SQL : ' . $e->getMessage() . '<br/>';
        die();
    }
    return $rowAllFHF;
}