<?php

?>

<!DOCTYPEhtml>
<head>
    <title>GSB Notes de frais</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="CSS/Style.css"/>
    <link rel="stylesheet" media="screen and (max-width: 1000px)" href="CSS/Style_1000px.css"/>
    <link rel="shortcut icon" type="image" href="Images/Logo3.jpg" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
</head>

<body>

<div class="wrapper">

    <header class="header">
        <img id="grande" src="Images/Logo2.png" alt="logo">
        <form method="POST" id="accueil" action="./index.php?p=login">
            <div id="connexionButton"><input id="buttonLogin" type="submit" name="connexion"></div>
            <div id="identifiants">
                <input type="text" placeholder="Identifiant" name="pseudo" id="pseudo" size="10" required><br/>
                <input type="password" placeholder="Mot de passe" name="pass" id="pass" size="10" required ><br/>
            </div>
        </form>
        <i class="fas fa-user"></i>
    </header><!-- .header-->

    <div class="middle">

        <div class="container">
            <main class="content">

            </main><!-- .content -->
        </div><!-- .container-->

        <aside class="left-sidebar" style="display: none">
        </aside><!-- .left-sidebar -->

    </div><!-- .middle-->

</div><!-- .wrapper -->

<footer class="footer">
    <a id="footer" href="./index.php?p=histoire">Histoire de GSB</a>
    <text>All rights reserved ©</text>
</footer><!-- .footer -->

</body>

