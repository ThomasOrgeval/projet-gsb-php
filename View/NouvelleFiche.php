<?php

if (isset($_POST['buttonV'])) {
    queryNumeroFF();
}

if (isset($_POST['buttonV2'])) {
    queryNumeroFHF();
}

?>

<!DOCTYPEhtml>
<html>
<head>
    <title>GSB Notes de frais</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="CSS/Style.css"/>
    <link rel="stylesheet" media="screen and (max-width: 1000px)" href="CSS/Style_1000px.css"/>
    <link rel="stylesheet" href="CSS/Theme/<?= $_SESSION['theme']?>.css"/>
    <link rel="shortcut icon" type="image" href="Images/Logo3.jpg"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
          integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
</head>

<body>

<div class="wrapper">

    <header class="header">
        <img id="grande" src="Images/Logo2.png" alt="logo">
        <a id="logout" href="Controller/Logout.php">
            <button id="logout">
                <img id="logout" src="Images/Logout.svg" href="../Controller/Logout.php">
                <logoutText>Déconnexion</logoutText>
            </button>
        </a>
    </header><!-- .header-->

    <div class="middle">

        <div class="container">
            <main class="content">
                <form method="POST">
                    <h1>Frais forfaitaires :</h1><br/><br/>
                    <label for="ForfaitEtape">Forfait étape :</label>
                    <input type="text" name="ForfaitEtape" id="ForfaitEtape"/><br/>

                    <label for="FraisKm">Forfait kilométrique :</label>
                    <input type="number" name="FraisKm" id="FraisKm" placeholder="Nombre de"/>
                    <label>km</label><br/>

                    <label for="Nuit">Nuités hôtel :</label>
                    <input type="number" name="Nuit" id="Nuit" placeholder="Nombre de"/>
                    <label>nuits</label><br/>

                    <label for="Repas">Repas restaurant :</label>
                    <input type="number" name="Repas" id="Repas" placeholder="Nombre de"/>
                    <label>repas</label><br/><br/>
                    <br/>

                    <a><input class="button1" type="submit" value="Valider" name="buttonV"></a><br/><br/><br/>

                    <h1>Autres frais :</h1><br/><br/>

                    <c>
                        <div class="article">
                            <label>Le </label>
                            <input type="date" name="the_date1">
                            <label>-</label>
                            <input type="text" name="AutreForfais1" id="HorsForfait" placeholder="Libellé"/>
                            <label>-</label>
                            <input type="number" name="PrixHF1" id="PrixHF" placeholder="Prix"/>
                            <label>€</label><br/>

                            <label>Le </label>
                            <input type="date" name="the_date2">
                            <label>-</label>
                            <input type="text" name="AutreForfais2" id="HorsForfait" placeholder="Libellé"/>
                            <label>-</label>
                            <input type="number" name="PrixHF2" id="PrixHF" placeholder="Prix"/>
                            <label>€</label><br/><br/>
                        </div>

                    </c>
                    <br/><br/><br/>


                    <a><input class="button1" type="submit" value="Valider" name="buttonV2"></a>
                </form>
            </main><!-- .content -->
        </div><!-- .container-->

        <aside class="left-sidebar">
            <div class="btn-group">
                <a id="leftButton" href="index.php?p=profil">
                    <button id="leftButton">
                        <img id="sidebar" src="Images/Accueil.svg">
                        <buttonText>Accueil</buttonText>
                    </button>
                </a>
                <a id="leftButton" href="./index.php?p=consulter">
                    <button id="leftButton">
                        <img id="sidebar" src="Images/ConsultationFiche.svg">
                        <buttonText>Consulter vos fiches de frais</buttonText>
                    </button>
                </a>
                <a id="leftButton" href="./index.php?p=themes">
                    <button id="leftButton">
                        <img id="sidebar" src="Images/Themes.svg">
                        <buttonText>Choisir un thème</buttonText>
                    </button>
                </a>
            </div>
        </aside><!-- .left-sidebar -->

    </div><!-- .middle-->

</div><!-- .wrapper -->

<footer class="footer">
    <text style="float: left;
                         margin-left: 2rem;">id : <?php echo $_SESSION['id'] ?></text>
    <text>All rights reserved ©</text>
</footer><!-- .footer -->

</body>
</html>