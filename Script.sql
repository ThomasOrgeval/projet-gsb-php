drop database if exists gsbV2;
create database gsbV2;
use gsbV2;

create table FraisForfait
(
    ID      char(3)      not null,
    Libelle varchar(255) not null,
    Montant double       not null,
    primary key (ID)
)ENGINE=InnoDB;


create table Etat
(
    ID_Etat char(2)      not null,
    Libelle varchar(255) not null,
    primary key (ID_Etat)
)ENGINE=InnoDB;

create table Visiteur
(
    ID_Visiteur  varchar(4)   not null,
    Nom          varchar(255) not null,
    Prenom       varchar(255) not null,
    Login        varchar(255) not null,
    Mdp          varchar(255) not null,
    Adresse      varchar(255) not null,
    CP           char(5)      not null,
    Ville        varchar(255) not null,
    DateEmbauche date         not null,
    primary key (ID_Visiteur)
)ENGINE=InnoDB;

create table FicheFrais
(
    ID_Visiteur     varchar(4) not null,
    Mois            int        not null,
    NbJustificatifs int        not null,
    MontantValide   double     not null,
    DateModif       date       not null,
    ID_Etat         char(2)    not null,
    primary key (ID_Visiteur, Mois)
)ENGINE=InnoDB;

create table LigneFraisForfait
(
    ID_Visiteur     varchar(4) not null,
    Mois            int        not null,
    ID_FraisForfait char(3)    not null,
    Quantite        int        not null,
    primary key (ID_Visiteur, Mois, ID_FraisForfait)
)ENGINE=InnoDB;

create table LigneFraisHorsForfait
(
    ID_LFHF     int auto_increment not null,
    ID_Visiteur varchar(4)         not null,
    Mois        int                not null,
    Libelle     varchar(255)       not null,
    Date1       date               not null,
    Montant     double             not null,
    primary key (ID_LFHF)
)ENGINE=InnoDB;

alter table FicheFrais
    add constraint foreign key (ID_Etat) references Etat (ID_Etat);
alter table FicheFrais
    add constraint foreign key (ID_Visiteur) references Visiteur (ID_Visiteur);

alter table LigneFraisForfait
    add constraint foreign key (ID_Visiteur, Mois) references FicheFrais (ID_Visiteur, Mois);
alter table LigneFraisForfait
    add constraint foreign key (ID_FraisForfait) references FraisForfait (ID);

alter table LigneFraisHorsForfait
    add constraint foreign key (ID_Visiteur, Mois) references FicheFrais (ID_Visiteur, Mois);


INSERT INTO FraisForfait (ID, Libelle, Montant)
VALUES ('ETP', 'Forfait Etape', 110.00),
       ('KM', 'Frais Kilométrique', 0.62),
       ('NUI', 'Nuitée Hôtel', 80.00),
       ('REP', 'Repas Restaurant', 25.00);


INSERT INTO Etat (ID_Etat, Libelle)
VALUES ('RB', 'Remboursée'),
       ('CL', 'Saisie clôturée'),
       ('CR', 'Fiche créée, saisie en cours'),
       ('VA', 'Validée et mise en paiement');


INSERT INTO Visiteur (ID_Visiteur, Nom, Prenom, Login, MDP, Adresse, CP, Ville, DateEmbauche)
VALUES ('a131', 'Villechalane', 'Louis', 'lvillachane', 'jux7g', '8 rue des Charmes', '46000', 'Cahors', '2005-12-21'),
       ('a17', 'Andre', 'David', 'dandre', 'oppg5', '1 rue Petit', '46200', 'Lalbenque', '1998-11-23'),
       ('a55', 'Bedos', 'Christian', 'cbedos', 'gmhxd', '1 rue Peranud', '46250', 'Montcuq', '1995-01-12'),
       ('a93', 'Tusseau', 'Louis', 'ltusseau', 'ktp3s', '22 rue des Ternes', '46123', 'Gramat', '2000-05-01'),
       ('b13', 'Bentot', 'Pascal', 'pbentot', 'doyw1', '11 allée des Cerises', '46512', 'Bessines', '1992-07-09'),
       ('b16', 'Bioret', 'Luc', 'lbioret', 'hrjfs', '1 Avenue gambetta', '46000', 'Cahors', '1998-05-11'),
       ('b19', 'Bunisset', 'Francis', 'fbunisset', '4vbnd', '10 rue des Perles', '93100', 'Montreuil', '1987-10-21'),
       ('b25', 'Bunisset', 'Denise', 'dbunisset', 's1y1r', '23 rue Manin', '75019', 'paris', '2010-12-05'),
       ('b28', 'Cacheux', 'Bernard', 'bcacheux', 'uf7r3', '114 rue Blanche', '75017', 'Paris', '2009-11-12'),
       ('b34', 'Cadic', 'Eric', 'ecadic', '6u8dc', '123 avenue de la République', '75011', 'Paris', '2008-09-23'),
       ('b4', 'Charoze', 'Catherine', 'ccharoze', 'u817o', '100 rue Petit', '75019', 'Paris', '2005-11-12'),
       ('b50', 'Clepkens', 'Christophe', 'cclepkens', 'bw1us', '12 allée des Anges', '93230', 'Romainville',
        '2003-08-11'),
       ('b59', 'Cottin', 'Vincenne', 'vcottin', '2hoh9', '36 rue Des Roches', '93100', 'Monteuil', '2001-11-18'),
       ('c14', 'Daburon', 'François', 'fdaburon', '7oqpv', '13 rue de Chanzy', '94000', 'Créteil', '2002-02-11'),
       ('c3', 'De', 'Philippe', 'pde', 'gk9kx', '13 rue Barthes', '94000', 'Créteil', '2010-12-14'),
       ('c54', 'Debelle', 'Michel', 'mdebelle', 'od5rt', '181 avenue Barbusse', '93210', 'Rosny', '2006-11-23'),
       ('d13', 'Debelle', 'Jeanne', 'jdebelle', 'nvwqq', '134 allée des Joncs', '44000', 'Nantes', '2000-05-11'),
       ('d51', 'Debroise', 'Michel', 'mdebroise', 'sghkb', '2 Bld Jourdain', '44000', 'Nantes', '2001-04-17'),
       ('e22', 'Desmarquest', 'Nathalie', 'ndesmarquest', 'f1fob', '14 Place d Arc', '45000', 'Orléans', '2005-11-12'),
       ('e24', 'Desnost', 'Pierre', 'pdesnost', '4k2o5', '16 avenue des Cèdres', '23200', 'Guéret', '2001-02-05'),
       ('e39', 'Dudouit', 'Frédéric', 'fdudouit', '44im8', '18 rue de l église', '23120', 'GrandBourg', '2000-08-01'),
       ('e49', 'Duncombe', 'Claude', 'cduncombe', 'qf77j', '19 rue de la tour', '23100', 'La souteraine', '1987-10-10'),
       ('e5', 'Enault-Pascreau', 'Céline', 'cenault', 'y2qdu', '25 place de la gare', '23200', 'Gueret', '1995-09-01'),
       ('e52', 'Eynde', 'Valérie', 'veynde', 'i7sn3', '3 Grand Place', '13015', 'Marseille', '1999-11-01'),
       ('f21', 'Finck', 'Jacques', 'jfinck', 'mpb3t', '10 avenue du Prado', '13002', 'Marseille', '2001-11-10'),
       ('f39', 'Frémont', 'Fernande', 'ffremont', 'xs5tq', '4 route de la mer', '13012', 'Allauh', '1998-10-01'),
       ('f4', 'Gest', 'Alain', 'agest', 'dywvt', '30 avenue de la mer', '13025', 'Berre', '1985-11-01');

insert into LigneFraisForfait (ID_Visiteur, Mois, ID_FraisForfait, Quantite)
values ('a131', 4, 'ETP', 25),
       ('a17', 4, 'ETP', 25),
       ('a17', 4, 'NUI', 25),
       ('a131', 5, 'NUI', 70),
       ('a17', 4, 'REP', 25),
       ('a131', 4, 'REP', 90);

insert into LigneFraisHorsForfait (ID_LFHF, ID_Visiteur, Mois, Libelle, Date1, Montant)
values (1, 'a131', 4, 'le fromage', '2018-04-20', 50.28);

/*drop user if exists Responsables;
drop user if exists Visiteurs; 

-- Reponsables
drop user if exists Comptables;
CREATE USER 'Responsables' IDENTIFIED BY 'mdpResponsable';
GRANT ALL PRIVILEGES ON gsbv2.* TO 'Responsables'@'%' identified by 'mdpResponsable';

/*-- Visiteurs
CREATE USER 'Visiteurs' IDENTIFIED BY 'mdpUser';
GRANT INSERT ON FraisForfait TO 'Visiteurs'@'%' identified by 'mdpUser';

grant select on Etat to Visiteurs;
grant select on LigneFraisForfait to Visiteurs;
grant select on LigneFraisHorsForfait to Visiteurs;

-- Comptables
CREATE USER 'Comptables' IDENTIFIED BY 'mdpCompable';
GRANT ALL PRIVILEGES ON * . * TO 'Comptables';

flush privileges;*/