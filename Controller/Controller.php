<?php

// Changement de pages

session_start();
require("./Model/Model.php");

function accueil()
{
    require('./View/Accueil.php');
    session_destroy();
}

function userExist()
{
    if (isset($_POST['pseudo']) && isset($_POST['pass'])) {

        $statement = login();
        if ($statement == true) {
            $_SESSION['theme'] = 1;
            $_SESSION['nom'] = $statement['Nom'];
            $_SESSION['id'] = $statement['ID_Visiteur'];
            $_SESSION['prenom'] = $statement['Prenom'];
            require('./View/Identification.php');

        } else {
            echo 'Mot de passe ou identifiant incorrect : Code 01';

        }
    }
    if (!(session_id())) {
        echo "Mot de passe ou identifiant incorrect : Code 02";
    }
}

function account()
{
    require('./View/Identification.php');
}

function nouvelleFIche()
{
    require 'View/NouvelleFiche.php';
}

function consulterFiche()
{
    require './View/ConsultationFiche.php';

}

function resultat()
{
    require './View/Resultat.php';
}

function histoire()
{
    require './View/Histoire.html';
}

function theme()
{
    require './View/Themes.php';
}

// Rêquete création de fiche (FF=Frais forfait et FHF=Frais hors forfait)

function queryNumeroFF()
{
    $ligne = ligneFF();
    $bdd = db();

    $id = $_SESSION['id'];
    $date = date("m");
    $ForfaitEtape = ($_POST['ForfaitEtape']);
    $FraisKm = ($_POST['FraisKm']);
    $Nuit = ($_POST['Nuit']);
    $Repas = ($_POST['Repas']);

    @$ligne = $ligne[0] + 1;
    $num = $ligne;
    $WB = false;
    $i = 0;
    while ($WB == false) {
        $i++;
        $num = $ligne;
        $Vnum = $ligne;
        if ($i == 1) {
            $afficher = $ForfaitEtape;
            $num .= "FE";
        } else if ($i == 2) {
            $afficher = $FraisKm;
            $num .= "F";
        } else if ($i == 3) {
            $afficher = $Nuit;
            $num .= "N";
        } else if ($i == 4) {
            $afficher = $Repas;
            $num .= "R";
        }

        $bdd->exec("INSERT INTO lignefraisforfait(ID_Visiteur, Mois, ID_FraisForfait, Quantite) VALUES ('$id','$date','$num','$afficher')");
        if ($num == $Vnum .= "R") {
            $WB = true;
        }
    }
}

function queryNumeroFHF()
{
    $bdd = db();

    $id = $_SESSION['id'];
    $datem = date("m");
    $date1 = ($_POST["the_date1"]);
    $date2 = ($_POST["the_date2"]);
    $Forfait1 = ($_POST['AutreForfais1']);
    $Forfait2 = ($_POST['AutreForfais2']);
    $PrixHF1 = ($_POST['PrixHF1']);
    $PrixHF2 = ($_POST['PrixHF2']);

    $bdd->exec("INSERT INTO lignefraishorsforfait(ID_Visiteur, Mois, Libelle, Date1, Montant) VALUES ('$id','$datem','$Forfait1','$date1','$PrixHF1')");
    $bdd->exec("INSERT INTO lignefraishorsforfait(ID_Visiteur, Mois, Libelle, Date1, Montant) VALUES ('$id','$datem','$Forfait2','$date2','$PrixHF2')");

}

// Rêquete consultation de fiche

function queryConsulter()
{
    $NbreDataFF = donneesConsulterNbrFF();
    $rowAllFF = donneesConsulterRowFF();

    $NbreDataFHF = donnneesConsulterNbrFHF();
    $rowAllFHF = donnneesConsulterRowFHF();


// --------------------------------
// affichage
    if (($NbreDataFF + $NbreDataFHF) != 0) {
        ?>
        <table border="1" id="table">
            <thead>
            <tr>
                <th>FraisForfait</th>
                <th>Quantite</th>
            </tr>
            </thead>
            <tbody>
            <?php
            // pour chaque ligne (chaque enregistrement)
            foreach ($rowAllFF as $row) {
                // DONNEES A AFFICHER dans chaque cellule de la ligne
                ?>
                <tr>
                    <td>
                        <?php
                        $rest = substr($row['ID_FraisForfait'], 1);
                        if ($rest == "F") {
                            echo $rest = "Forfait kilométrique";
                        } elseif ($rest == "N") {
                            echo $rest = "Nuités hôtel";
                        } elseif ($rest == "R") {
                            echo $rest = "Repas restaurant";
                        } elseif ($rest == "FE") {
                            echo $rest = "Forfait étape";
                        }
                        ?>
                    </td>
                    <td><?= $row['Quantite']; ?></td>
                </tr>
                <?php
            } // fin foreach
            ?>
            </tbody>
        </table><br/><br/><br/>
        <table border="1" id="table">
            <thead>
            <tr>
                <th>Libelle</th>
                <th>Montant</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($rowAllFHF as $row) {
                ?>
                <tr>
                    <td><?= $row['Libelle']; ?></td>
                    <td><?= $row['Montant']; ?></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <?php
    } else { ?>
        pas de données à afficher
        <?php
    }
}

// Changement de thèmes

function queryTheme()
{
    $_SESSION['theme'] = $_POST['themeNum'];
    return $_SESSION['theme'];
}
