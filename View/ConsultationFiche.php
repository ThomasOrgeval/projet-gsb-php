<?php

?>

<!DOCTYPEhtml>
<html>
<head>
    <title>GSB Notes de frais</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="CSS/Style.css"/>
    <link rel="stylesheet" media="screen and (max-width: 1000px)" href="CSS/Style_1000px.css"/>
    <link rel="stylesheet" href="CSS/Theme/<?= $_SESSION['theme']?>.css"/>
    <link rel="shortcut icon" type="image" href="Images/Logo3.jpg"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
          integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
</head>

<body>

<div class="wrapper">

    <header class="header">
        <img id="grande" src="Images/Logo2.png" alt="logo">
        <a id="logout" href="Controller/Logout.php"> <!-- a changer par le logout.php-->
            <button id="logout">
                <img id="logout" src="Images/Logout.svg" href="../Controller/Logout.php">
                <logoutText>Déconnexion</logoutText>
            </button>
        </a>
    </header><!-- .header-->

    <div class="middle">

        <div class="container">
            <main class="content">
                <form method="post" action="./index.php?p=consulter2">
                    <p><label for="mois">Veuillez selectionner un mois :</label>
                        <select name="mois" id="mois">
                            <option value="1">Janvier</option>
                            <option value="2">Février</option>
                            <option value="3">Mars</option>
                            <option value="4">Avril</option>
                            <option value="5">Mai</option>
                            <option value="6">Juin</option>
                            <option value="7">Juillet</option>
                            <option value="8">Août</option>
                            <option value="9">Septembre</option>
                            <option value="10">Octobre</option>
                            <option value="11">Novembre</option>
                            <option value="12">Décembre</option>
                        </select>
                    </p>
                    <br/>
                    <a><input class="button1" type="submit" value="Valider" name="buttonAfficher"></a>
                </form>
            </main><!-- .content -->
        </div><!-- .container-->

        <aside class="left-sidebar">
            <div class="btn-group">
                <a id="leftButton" href="index.php?p=profil">
                    <button id="leftButton">
                        <img id="sidebar" src="Images/Accueil.svg">
                        <buttonText>Accueil</buttonText>
                    </button>
                </a>
                <a id="leftButton" href="./index.php?p=nouvelle">
                    <button id="leftButton">
                        <img id="sidebar" src="Images/NouvelleFiche.svg">
                        <buttonText>Créer une nouvelle fiche de frais</buttonText>
                    </button>
                </a>
                <a id="leftButton" href="./index.php?p=themes">
                    <button id="leftButton">
                        <img id="sidebar" src="Images/Themes.svg">
                        <buttonText>Choisir un thème</buttonText>
                    </button>
                </a>
            </div>
        </aside><!-- .left-sidebar -->

    </div><!-- .middle-->

</div><!-- .wrapper -->

<footer class="footer">
    <text style="float: left;
                         margin-left: 2rem;">id : <?php echo $_SESSION['id'] ?></text>
    <text>All rights reserved ©</text>
</footer><!-- .footer -->

</body>
</html>